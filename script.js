/* Відповідь 1: 
функції є важливим інструментом у програмуванні, який сприяє повторному використанню коду,
поліпшенню організації програм та забезпеченню читабельності та розуміння коду.*/

/* Відповідь 2: 
Передача аргументів у функцію дозволяє керувати вхідними даними, забезпечує гнучкість,
параметризацію та реюзабельність, що є важливими принципами програмування.*/

/* Відповідь 3: 
Оператор return у програмуванні використовується для повернення значення з функції.
Він дозволяє передати результат обчислення або об'єкт назад у ту частину програми, де функція була викликана.

Коли оператор return виконується всередині функції, він припиняє виконання функції та повертає управління до місця виклику функції.
Крім того, він передає вказане значення або об'єкт як результат функції.*/

//Задача 1:

const getNumbers = () => {
  const num1 = +prompt("Введіть перше число:");
  const num2 = +prompt("Введіть друге число:");

  if (isNaN(num1) || isNaN(num2)) {
    console.log("Будь ласка, введіть числа!");
    return null;
  }
  return [num1, num2];
};

const getOperation = () => {
  const operation = prompt("Введіть математичну операцію (+, -, *, /):");

  if (
    operation !== "+" &&
    operation !== "-" &&
    operation !== "*" &&
    operation !== "/"
  ) {
    console.log("Будь ласка, введіть коректну операцію!");
    return null;
  }
  return operation;
};

const calculate = (num1, num2, operation) => {
  let result;

  switch (operation) {
    case "+":
      result = num1 + num2;
      break;
    case "-":
      result = num1 - num2;
      break;
    case "*":
      result = num1 * num2;
      break;
    case "/":
      result = num1 / num2;
      break;
    default:
      console.log("Введено некоректну операцію.");
      break;
  }
  return result;
};

const numbers = getNumbers();
const operation = getOperation();

if (numbers !== null && operation !== null) {
  const [num1, num2] = numbers;
  const result = calculate(num1, num2, operation);
  console.log("Результат:", result);
}
